package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.api.endpoint.Project;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TaskTest extends AbstractTest {

    @After
    public void after() throws Exception {
        taskEndpoint.removeAllTasksByUserId(tokenUser, userId);
        taskEndpoint.removeAllTasksByUserId(tokenAdmin, adminId);
        projectEndpoint.removeAllProjectByUserId(tokenUser, userId);
        projectEndpoint.removeAllProjectByUserId(tokenAdmin, adminId);
    }

    @Test
    public void findAllTasks() throws Exception {
        @NotNull final Task task1 = createTask(userId);
        @NotNull final Task task2 = createTask(adminId);
        @NotNull final Task task3 = createTask(userId);
        task3.setName("Task 3");
        taskEndpoint.persistTask(tokenUser, task1);
        taskEndpoint.persistTask(tokenAdmin, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        @NotNull final List<Task> userTasks = taskEndpoint.findAllTasksByUserId(tokenUser, userId);
        @NotNull final List<Task> adminTasks = taskEndpoint.findAllTasksByUserId(tokenUser, adminId);
        Assert.assertTrue(userTasks.size() + adminTasks.size() == 3);
    }

    @Test
    public void findAllTasksByUserId() throws Exception {
        @NotNull final Task task1 = createTask(userId);
        @NotNull final Task task2 = createTask(userId);
        @NotNull final Task task3 = createTask(userId);
        task3.setName("Task 3");
        taskEndpoint.persistTask(tokenUser, task1);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        @NotNull final List<Task> userTasks = taskEndpoint.findAllTasksByUserId(tokenUser, userId);
        Assert.assertTrue(userTasks.size() == 3);
    }

    @Test
    public void  findOneTask() throws Exception {
        @NotNull final Task task = createTask(userId);
        taskEndpoint.persistTask(tokenUser, task);
        assertEquals(task.getName(), taskEndpoint.findOneTask(tokenUser, task.getId()).getName());
    }

    @Test
    public void  findOneTaskByUserId() throws Exception {
        @NotNull final Task task = createTask(userId);
        taskEndpoint.persistTask(tokenUser,task);
        assertEquals(task.getName(), taskEndpoint.findOneTaskByUserId(tokenUser,
                userId, task.getId()).getName());
    }

    @Test
    public void persistTask() throws Exception {
        @NotNull final Task task = createTask(userId);
        taskEndpoint.persistTask(tokenUser,task);
        Assert.assertNotNull(taskEndpoint.findOneTask(tokenUser, task.getId()));
    }

    @Test
    public void mergeTask() throws Exception {
        @NotNull final Task task = createTask(userId);
        task.setName("testtest");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull Task testTask = taskEndpoint.findOneTask(tokenUser, task.getId());
        testTask.setDescription("test");
        taskEndpoint.mergeTask(tokenUser, testTask);
        Assert.assertEquals("test", testTask.getDescription());
    }

    @Test
    public void removeTaskById() throws Exception {
        @NotNull final Task task = createTask(userId);
        taskEndpoint.persistTask(tokenUser,task);
        @NotNull Task testTask = taskEndpoint.findOneTask(tokenUser, task.getId());
        Assert.assertEquals("TestTask", taskEndpoint.findOneTask(tokenUser, task.getId()).getName());
        taskEndpoint.removeTask(tokenUser, testTask.getId());
        Assert.assertNull(taskEndpoint.findOneTask(tokenUser, task.getId()));
    }

    @Test
    public void removeTaskByUserId() throws Exception {
        @NotNull final Task task = createTask(userId);
        taskEndpoint.persistTask(tokenUser,task);
        @NotNull Task testTask = taskEndpoint.findOneTask(tokenUser, task.getId());
        Assert.assertEquals("TestTask", taskEndpoint.findOneTask(tokenUser, task.getId()).getName());
        taskEndpoint.removeTaskByUserId(tokenUser, userId, testTask.getId());
        Assert.assertNull(taskEndpoint.findOneTask(tokenUser, task.getId()));
    }

    @Test
    public void removeAllTasks() throws Exception {
        @NotNull final Task task1 = createTask(userId);
        @NotNull final Task task2 = createTask(userId);
        @NotNull final Task task3 = createTask(userId);
        task3.setName("Task 3");
        taskEndpoint.persistTask(tokenUser, task1);
        taskEndpoint.persistTask(tokenAdmin, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        @NotNull final List<Task> userTasks = taskEndpoint.findAllTasksByUserId(tokenUser, userId);
        Assert.assertTrue(userTasks.size() == 3);
        taskEndpoint.removeAllTasksByUserId(tokenUser, userId);
        @NotNull final List<Task> userTasks1 = taskEndpoint.findAllTasksByUserId(tokenUser, userId);
        Assert.assertTrue(userTasks1.size() == 0);
    }

    @Test
    public void searchTask() throws Exception {
        @NotNull final Task task1 = createTask(userId);
        @NotNull final Task task2 = createTask(userId);
        @NotNull final Task task3 = createTask(userId);
        task3.setName("Lorem");
        task2.setDescription("Lorem ipsum");
        taskEndpoint.persistTask(tokenUser, task1);
        taskEndpoint.persistTask(tokenAdmin, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        @NotNull final List<Task> taskList = taskEndpoint.searchTask(tokenUser, userId, "Lorem");
        Assert.assertTrue(taskList.size() == 2);
    }

    @Test
    public void sortTask() throws Exception {
        @NotNull final Task task1 = createTask(userId);
        @NotNull final Task task2 = createTask(userId);
        @NotNull final Task task3 = createTask(userId);
        task3.setName("ALorem");
        task2.setName("BLorem");
        task1.setName("CLorem");
        taskEndpoint.persistTask(tokenUser, task1);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        @NotNull final List<Task> userTasks = taskEndpoint.findAllTasksByUserId(tokenUser, userId);
        @NotNull final List<Task> sortTasks = taskEndpoint.sortTask(userTasks, "");
        Assert.assertEquals("ALorem", sortTasks.get(0).getName());
        Assert.assertEquals("BLorem", sortTasks.get(1).getName());
        Assert.assertEquals("CLorem", sortTasks.get(2).getName());
    }

    private Task createTask(String userId) throws Exception {
        @NotNull final Project project = createProject(userId);
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull final Task task = taskEndpoint.insertTask(tokenUser, userId, "TestTask",
                "Description 123", "10.01.2002", "15.10.2005");
        task.setProjectId(project.getId());
        return task;
    }

    private Project createProject(String userId) throws Exception {
        @NotNull final Project project = projectEndpoint.insertProject(tokenUser, userId, "TestProject",
                "Description 123", "10.01.2002", "15.10.2005");
        return project;
    }

}