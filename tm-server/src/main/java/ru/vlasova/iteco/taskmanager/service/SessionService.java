package ru.vlasova.iteco.taskmanager.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.api.service.IPropertyService;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.util.AESUtil;
import ru.vlasova.iteco.taskmanager.util.HashUtil;
import ru.vlasova.iteco.taskmanager.util.SignatureUtil;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Setter
public final class SessionService implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private IUserService userService;

    @NotNull
    @Getter
    private IPropertyService propertyService;

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.userService = serviceLocator.getUserService();
        this.propertyService = serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public String getToken(@Nullable final String login, @Nullable String password) throws Exception {
        @Nullable final Session session = create(login,password);
        @NotNull final String key = serviceLocator.getPropertyService().getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return AESUtil.encrypt(json, key);
    }

    @Nullable
    @Override
    public Session create(@Nullable final String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findOne(userService.checkUser(login));
        if (user == null) return null;
        password = HashUtil.MD5(password);
        if (password == null) return null;
        if (!password.equals(user.getPwd())) throw new AccessDeniedException("Password incorrect");
        Session userSession = new Session();
        userSession.setUserId(user.getId());
        userSession.setRole(user.getRole());
        userSession.setSignature(SignatureUtil.sign(userSession,
                propertyService.getSessionSalt(),
                propertyService.getSessionCycle()));
        persist(userSession);
        return userSession;
    }

    @Override
    public void validate(@Nullable final String token) throws Exception {
        Session currentSession = decryptToken(token);
        final Date currentTime = new Date();
        if (currentSession == null) throw new AccessDeniedException("Invalid session");
        if (!contains(currentSession.getId())) throw new AccessDeniedException("Invalid session");
        if (currentSession.getUserId() == null) throw new AccessDeniedException("Invalid session");
        if (currentSession.getSignature() == null) throw new AccessDeniedException("Invalid session");
        if (currentSession.getRole() == null) throw new AccessDeniedException("Invalid session");
        @NotNull final Session session = new Session();
        session.setSignature(currentSession.getSignature());
        session.setRole(currentSession.getRole());
        session.setId(currentSession.getId());
        session.setCreateDate(currentSession.getCreateDate());
        session.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature = SignatureUtil.sign
                (session, propertyService.getSessionSalt(), propertyService.getSessionCycle());
        @Nullable final String currentSessionSignature = SignatureUtil.sign
                (currentSession, propertyService.getSessionSalt(), propertyService.getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null)
            throw new AccessDeniedException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new AccessDeniedException("Invalid session");
        if (currentSession.getCreateDate().getTime() - currentTime.getTime() > propertyService.getSessionLifetime())
            throw new AccessDeniedException("Invalid session");
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        @NotNull final List<Session> users = repository.findAll();
        session.commit();
        session.close();
        return users;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        @Nullable final Session userSession = repository.findOne(id);
        session.close();
        return userSession;
    }

    @Override
    public Session persist(@Nullable final Session userSession) throws Exception {
        if (userSession == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        try {
            repository.persist(userSession);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Session was not created");
        } finally {
            session.close();
        }
        return userSession;
    }

    @Override
    public void merge(@Nullable final Session userSession) throws Exception {
        if (userSession == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        try {
            repository.merge(userSession);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Session was not updated");
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String token) throws Exception {
        if (token == null) return;
        Session currentSession = decryptToken(token);
        if (currentSession.getSignature() == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        try {
            repository.removeSessionBySignature(currentSession.getSignature());
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Session was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Sessions was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public boolean contains(@Nullable final String sessionId) throws Exception {
        if (sessionId == null) return false;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = session.getMapper(ISessionRepository.class);
        @NotNull final boolean contains = repository.contains(sessionId);
        session.commit();
        session.close();
        return contains;
    }

    @Override
    public void checkSession(@Nullable final String token, @NotNull final Role role) throws Exception {
        if (token == null) throw new AccessDeniedException("Invalid session");
        validate(token);
        @NotNull final Session session = decryptToken(token);
        if (session.getRole() != role) throw new AccessDeniedException("Access denied");
    }

    private Session decryptToken(@Nullable final String token) throws Exception {
        @NotNull final String key = serviceLocator.getPropertyService().getSecretKey();
        @NotNull final String json = AESUtil.decrypt(token, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Session session = mapper.readValue(json, Session.class);
        return session;
    }

    @Override
    @Nullable
    public String getCurrentUserId(@Nullable final String token) throws Exception {
        @Nullable final Session currentSession = decryptToken(token);
        if (currentSession == null) throw new AccessDeniedException("Invalid session");
        return currentSession.getUserId();
    }

}
