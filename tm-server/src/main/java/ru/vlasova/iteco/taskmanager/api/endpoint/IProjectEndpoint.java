package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    Project insertProject(@Nullable String token, @Nullable final String userId,
                          @Nullable final String name, @Nullable final String description,
                          @Nullable final String dateStart, @Nullable final String dateFinish) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProjects(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProjectsByUserId(@Nullable final String token,
                                          @NotNull String userId) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProject(@Nullable final String token,
                           @NotNull String id) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProjectByUserId(@Nullable final String token,
                                   @NotNull String userId,
                                   @NotNull String id) throws Exception;

    @Nullable
    @WebMethod
    Project persistProject(@Nullable final String token,
                           @NotNull Project project) throws Exception;

    @WebMethod
    void mergeProject(@Nullable final String token,
                      @NotNull Project project) throws Exception;

    @WebMethod
    void removeProjectById(@Nullable final String token,
                           @NotNull String id) throws Exception;

    @WebMethod
    void removeProjectByUserId(@Nullable final String token,
                               @NotNull String userId,
                               @NotNull String id) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final String token) throws Exception;

    @WebMethod
    void removeAllProjectByUserId(@Nullable final String token,
                                  @NotNull String userId) throws Exception;

    @Nullable
    @WebMethod
    Project getProjectByIndex(@Nullable final String token,
                              @Nullable final String userId, int index) throws Exception;

    @Nullable
    @WebMethod
    List<Task> getTasksByProjectIndex(@Nullable final String token,
                                      @Nullable final String userId, int projectIndex) throws Exception;

    @Nullable
    @WebMethod
    List<Project> searchProject(@Nullable final String token,
                                @Nullable final String userId,
                                @Nullable final String searchString) throws Exception;

    @Nullable
    @WebMethod
    List<Project> sortProject(@NotNull final List<Project> projectList,
                              @Nullable final String sortMode) throws Exception;

}