package ru.vlasova.iteco.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.sql.SQLException;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public TaskService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void removeTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || projectId == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        repository.removeTasksByProjectId(userId, projectId);
        session.commit();
        session.close();
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || projectId == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final List<Task> taskList = repository.getTasksByProjectId(userId, projectId);
        session.commit();
        session.close();
        return taskList;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final String userId, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || userId == null) return null;
        @NotNull final Task task = new Task(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) throws Exception {
        if (userId == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = getTaskByIndex(userId, index);
        if (task == null) return;
        repository.removeByUserId(userId, task.getId());
        session.commit();
        session.close();
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeByUserId(userId,id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Task was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@Nullable final String userId, final int index) throws Exception {
        if (userId == null || index < 0 ) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final List<Task> taskList = repository.findAllByUserId(userId);
        session.commit();
        @Nullable final Task task = taskList.get(index);
        session.close();
        return task;
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String userId, @Nullable final String searchString) throws Exception {
        if (userId == null || searchString == null || searchString.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final List<Task> taskList = repository.search(userId, searchString);
        session.commit();
        session.close();
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) throws Exception {
        if (userId == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        session.commit();
        session.close();
        return taskList;
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.merge(entity);
            session.commit();
            return;
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(e);
        } finally {
            session.close();
        }
    }

    @Override
    @Nullable
    public Task persist(@Nullable final Task task) throws Exception {
        if (task == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.persist(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Task was not created");
        } finally {
            session.close();
        }
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final List<Task> taskList = repository.findAll();
        session.commit();
        session.close();
        return taskList;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = repository.findOne(id);
        session.commit();
        session.close();
        return task;
    }

    @Override
    @Nullable
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = repository.findOneByUserId(userId,id);
        session.commit();
        session.close();
        return task;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Task was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Tasks was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAllByUserId(userId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Tasks was not removed");
        } finally {
            session.close();
        }
    }

}
