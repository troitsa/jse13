package ru.vlasova.iteco.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@JsonAutoDetect
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @Nullable
    protected String userId;

    @Nullable
    @XmlElement(name = "project")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    private List<Task> tasks;

    @Nullable
    @XmlElement(name = "user")
    private List<User> users;

}
