package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    void binarySave(@Nullable final String token) throws Exception;

    @WebMethod
    void binaryLoad(@Nullable final String token) throws Exception;

    @WebMethod
    void fasterJsonLoad(@Nullable final String token) throws Exception;

    @WebMethod
    void fasterJsonSave(@Nullable final String token) throws Exception;

    @WebMethod
    void jaxBJsonLoad(@Nullable final String token) throws Exception;

    @WebMethod
    void jaxBJsonSave(@Nullable final String token) throws Exception;

    @WebMethod
    void fasterXmlLoad(@Nullable final String token) throws Exception;

    @WebMethod
    void fasterXmlSave(@Nullable final String token) throws Exception;

    @WebMethod
    void jaxBXmlLoad(@Nullable final String token) throws Exception;

    @WebMethod
    void jaxBXmlSave(@Nullable final String token) throws Exception;

    @Nullable
    String getSaveDir();

}
