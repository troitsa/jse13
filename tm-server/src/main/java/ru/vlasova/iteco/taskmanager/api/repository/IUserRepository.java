package ru.vlasova.iteco.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Nullable
    @Select("select id from taskmanager.app_user where login = #{login}")
    String checkUser(@NotNull @Param("login") final String login) throws Exception;

    @NotNull
    @Select("select * from taskmanager.app_user")
    List<User> findAll() throws Exception;

    @Nullable
    @Select("select * from taskmanager.app_user where id = #{id}")
    User findOne(@NotNull @Param("id") final String id) throws Exception;

    @Insert("insert into taskmanager.app_user values (#{id}, #{login}, #{pwd}, #{role})")
    void persist(@NotNull final User obj) throws DuplicateException, SQLException;

    @Update("update taskmanager.app_user set login = #{login}, pwd = #{pwd}, role = #{role} where id=#{id}")
    void merge(@NotNull final User obj) throws SQLException;

    @Delete("delete from taskmanager.app_user where id = #{id}")
    void remove(@NotNull @Param("id") final String id) throws SQLException;

    @Delete("delete from taskmanager.app_user")
    void removeAll() throws SQLException;

}
