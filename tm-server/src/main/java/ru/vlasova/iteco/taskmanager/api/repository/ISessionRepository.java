package ru.vlasova.iteco.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    @NotNull
    @Select("select * from taskmanager.app_session")
    List<Session> findAll() throws SQLException;

    @Nullable
    @Select("select * from taskmanager.app_project where id = #{id}")
    Session findOne(@NotNull @Param("id") final String id) throws SQLException;

    @Insert("insert into taskmanager.app_session " +
            "values (#{id}, #{userId}, #{role}, #{signature}, #{createDate})")
    void persist(@NotNull final Session obj) throws Exception;

    @Update("update taskmanager.app_session set user_Id = #{userId}, " +
            "role = #{role},  signature = #{signature}, createDate = #{createDate}" +
            "WHERE id = #{id}")
    void merge(@NotNull final Session obj) throws Exception;

    @Delete("delete from taskmanager.app_session where id = #{id}")
    void remove(@NotNull @Param("id") final String id) throws SQLException;

    @Delete("delete from taskmanager.app_session")
    void removeAll() throws SQLException;

    @Delete("delete from taskmanager.app_session where signature = #{signature}")
    void removeSessionBySignature(@NotNull @Param("signature") final String signature) throws SQLException;

    @Select("select exists (select id from taskmanager.app_session where id = #{id})")
    boolean contains(@NotNull @Param("id") final String sessionId) throws SQLException;

}
