package ru.vlasova.iteco.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private IUserService userService;

    public UserEndpoint() {
        super();
    }

    public UserEndpoint(@NotNull final ISessionService sessionService,
                        @NotNull final IUserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @Override
    public @Nullable User insertUser(@Nullable final String login,
                                     @Nullable final String password) {
        return userService.insert(login, password);
    }

    @Override
    public @Nullable User doLogin(@Nullable final String login,
                                  @Nullable final String password) throws Exception {
        return userService.doLogin(login, password);
    }

    @Override
    public @Nullable String checkUser(@Nullable final String token,
                                      @Nullable final String login) throws Exception {
        validateSession(token);
        return userService.checkUser(login);
    }

    @Override
    public void editUser(@Nullable final String token,
                         @Nullable final User user,
                         @Nullable final String login,
                         @Nullable final String password) throws Exception {
        validateSession(token);
        userService.edit(user, login, password);
    }

    @Override
    public @Nullable List<User> findAllUsers(@Nullable final String token) throws Exception {
        validateSession(token);
        return userService.findAll();
    }

    @Override
    public @Nullable User findUserBySession(@Nullable final String token,
                                      @Nullable final String id) throws Exception {
        validateSession(token);
        return userService.findOne(id);
    }

    @Override
    public @Nullable User findUser(@Nullable final String id) throws Exception {
        return userService.findOne(id);
    }

    @Override
    public @Nullable User persistUser(@Nullable final User user) throws Exception {
        return userService.persist(user);
    }

    @Override
    public void mergeUser(@Nullable final String token,
                          @Nullable final User user) throws Exception {
        validateSession(token);
        userService.merge(user);
    }

    @Override
    public void removeUser(@Nullable final String token,
                           @Nullable final String id) throws Exception {
        userService.remove(id);
    }

    @Override
    public void removeAllUsers(@Nullable final String token) throws Exception {
        validateSession(token);
        userService.removeAll();
    }

    @Override
    public Boolean checkRole(@Nullable String userId, @NotNull List<Role> roles) throws Exception {
        return userService.checkRole(userId, roles);
    }

}
