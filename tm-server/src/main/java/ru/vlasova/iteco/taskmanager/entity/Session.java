package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public class Session extends AbstractEntity {

    @Getter
    @Setter
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String userId;

    @Nullable
    private Role role;

    @Nullable
    private String signature;

    @NotNull
    private Date createDate = new Date(System.currentTimeMillis());

    @Override
    protected Object clone() {
        Session session = new Session();
        session.userId = this.userId;
        session.role = this.role;
        session.signature = this.signature;
        session.createDate = this.createDate;
        return session;
    }

}
