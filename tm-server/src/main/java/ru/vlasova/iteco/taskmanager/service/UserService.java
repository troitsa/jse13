package ru.vlasova.iteco.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void edit(@Nullable final User user, @Nullable final String login, @Nullable final String password) throws Exception {
        if (!isValid(login, password) || user == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        user.setLogin(login);
        user.setPwd(HashUtil.MD5(password));
        try {
            repository.merge(user);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("User was not updated");
        } finally {
            session.close();
        }
    }

    @Override
    @Nullable
    public User insert(@Nullable final String login, @Nullable final String password) {
        final boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    @Override
    @Nullable
    public User doLogin(final String login, final String password) throws Exception {
        @Nullable final String id = checkUser(login);
        if (id == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @Nullable final User user = repository.findOne(id);
        session.commit();
        session.close();
        if (user == null) return null;
        @Nullable final String pwd = user.getPwd();
        if (pwd == null) return null;
        if (pwd.equals(HashUtil.MD5(password))) {
            return user;
        } else {
            throw new AccessDeniedException("User or password incorrect");
        }
    }

    @Override
    @Nullable
    public String checkUser(final String login) throws Exception {
        if (!isValid(login)) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @Nullable String id = repository.checkUser(login);
        session.commit();
        session.close();
        if (id == null) return null;
        return id;
    }

    @Override
    public boolean checkRole(@Nullable final String userId, @Nullable final List<Role> roles) throws Exception {
        if (userId == null || roles == null) return false;
        @Nullable User user = findOne(userId);
        if (user == null) return false;
        for (@Nullable Role role : roles) {
            if (user.getRole().equals(role)) return true;
        }
        return false;
    }

    @Override
    public void merge(@Nullable final User entity) throws Exception {
        if (entity == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        repository.merge(entity);
        session.commit();
        session.close();
    }

    @Override
    @Nullable
    public User persist(@Nullable final User entity) throws Exception {
        if (entity == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        repository.persist(entity);
        session.commit();
        session.close();
        return entity;
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @NotNull final List<User> users = repository.findAll();
        session.commit();
        session.close();
        return users;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @Nullable final User user = repository.findOne(id);
        session.commit();
        session.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("User was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("User was not removed");
        } finally {
            session.close();
        }
    }

}
