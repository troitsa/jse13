package ru.vlasova.iteco.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Delete("delete from taskmanager.app_task where user_id = #{userId} and project_id = #{projectId}")
    void removeTasksByProjectId(@NotNull @Param("userId") final String userId,
                                @NotNull @Param("projectId") final String projectId) throws SQLException;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Result(property = "projectId", column = "project_id")
    @Select("select * from taskmanager.app_task where user_id = #{userId} and project_id = #{projectId}")
    List<Task> getTasksByProjectId(@NotNull @Param("userId") final String userId,
                                   @NotNull @Param("projectId") final String projectId) throws Exception;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Result(property = "projectId", column = "project_id")
    @Select("select * from taskmanager.app_task where user_id = #{userId} " +
            "and (name LIKE '%' #{searchString} '%' or description LIKE '%' #{searchString} '%')")
    List<Task> search(@NotNull @Param("userId") final String userId,
                      @NotNull @Param("searchString") final String searchString) throws Exception;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Result(property = "projectId", column = "project_id")
    @Select("select * from taskmanager.app_task where user_id = #{userId}")
    List<Task> findAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Result(property = "projectId", column = "project_id")
    @Select("select * from taskmanager.app_task where user_id = #{userId} and id = #{id}")
    Task findOneByUserId(@NotNull @Param("userId") final String userId,
                         @NotNull @Param("id") final String id) throws Exception;

    @Delete("delete from taskmanager.app_task where user_id = #{userId}")
    void removeAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Delete("delete from taskmanager.app_task where user_id = #{userId} and id = #{id}")
    void removeByUserId(@NotNull @Param("userId") final String userId,
                @NotNull @Param("id") final String id) throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Result(property = "projectId", column = "project_id")
    @Select("select * from taskmanager.app_task")
    List<Task> findAll() throws Exception;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Result(property = "projectId", column = "project_id")
    @Select("select * from taskmanager.app_task where id = #{id}")
    Task findOne(@NotNull @Param("id") final String id) throws Exception;

    @Insert("insert into taskmanager.app_task " +
            "values (#{id},#{userId},#{name},#{description}, " +
            "#{dateCreate},#{dateStart},#{dateFinish},#{status},#{projectId})")
    void persist(@NotNull final Task obj) throws Exception;

    @Update("update taskmanager.app_task set user_id=#{userId}," +
            "name = #{name}, description = #{description}, " +
            "dateCreate = #{dateCreate}, dateStart= #{dateStart}, " +
            "dateFinish =#{dateFinish}, status = #{status}, project_id=#{projectId}  where id = #{id}")
    void merge(@NotNull final Task obj) throws SQLException;

    @Delete("delete from taskmanager.app_task where id = #{id}")
    void remove(@NotNull @Param("id") final String id) throws SQLException;

    @Delete("delete from taskmanager.app_task")
    void removeAll() throws SQLException;

}
