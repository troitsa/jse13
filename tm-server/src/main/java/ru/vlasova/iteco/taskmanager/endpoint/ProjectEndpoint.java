package ru.vlasova.iteco.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.comparator.*;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull private IProjectService projectService;

    public ProjectEndpoint() {
        super();
    }

    public ProjectEndpoint(@NotNull final ISessionService sessionService,
                           @NotNull final IProjectService projectService) {
        super(sessionService);
        this.projectService = projectService;
    }

    @Override
    @Nullable
    public Project insertProject(@Nullable final String token, @Nullable final String userId, @Nullable final String name,
                                 @Nullable final String description, @Nullable final String dateStart,
                                 @Nullable final String dateFinish) throws Exception {
        validateSession(token);
        return projectService.insert(userId, name, description, dateStart, dateFinish);
    }

    @Override
    @Nullable
    public List<Project> findAllProjects(@Nullable final String token) throws Exception {
        validateSession(token);
        return projectService.findAll();
    }

    @Override
    @Nullable
    public List<Project> findAllProjectsByUserId(@Nullable final String token,
                                                 @NotNull final String userId) throws Exception {
        validateSession(token);
        return projectService.findAll(userId);
    }

    @Override
    public @Nullable Project findOneProject(@Nullable final String token,
                                            @NotNull final String id) throws Exception {
        validateSession(token);
        return projectService.findOne(id);
    }

    @Override
    public @Nullable Project findOneProjectByUserId(@Nullable final String token,
                                                    @NotNull final String userId,
                                                    @NotNull final String id) throws Exception {
        validateSession(token);
        return projectService.findOneByUserId(userId, id);
    }

    @Override
    public @Nullable Project persistProject(@Nullable final String token,
                                            @NotNull final Project project) throws Exception {
        validateSession(token);
        return projectService.persist(project);
    }

    @Override
    public void mergeProject(@Nullable final String token,
                             @NotNull final Project project) throws Exception {
        validateSession(token);
        projectService.merge(project);
    }

    @Override
    public void removeProjectById(@Nullable final String token,
                                  @NotNull final String id) throws Exception {
        validateSession(token);
        projectService.remove(id);
    }

    @Override
    public void removeProjectByUserId(@Nullable final String token,
                                      @NotNull final String userId,
                                      @NotNull final String id) throws Exception {
        validateSession(token);
        projectService.remove(userId, id);
    }

    @Override
    public void removeAllProjects(@Nullable final String token) throws Exception {
        validateSession(token);
        projectService.removeAll();
    }

    @Override
    public void removeAllProjectByUserId(@Nullable final String token,
                                         @NotNull final String userId) throws Exception {
        validateSession(token);
        projectService.removeAll(userId);
    }

    @Override
    public @Nullable Project getProjectByIndex(@Nullable final String token,
                                               @Nullable final String userId,
                                               int index) throws Exception {
        validateSession(token);
        return projectService.getProjectByIndex(userId, index);
    }

    @Override
    public @Nullable List<Task> getTasksByProjectIndex(@Nullable final String token,
                                                       @Nullable final String userId,
                                                       int projectIndex) throws Exception {
        validateSession(token);
        return projectService.getTasksByProjectIndex(userId, projectIndex);
    }

    @Override
    public @Nullable List<Project> searchProject(@Nullable final String token,
                                                 @Nullable final String userId,
                                                 @Nullable final String searchString) throws Exception {
        validateSession(token);
        return projectService.search(userId, searchString);
    }

    @Override
    public @Nullable List<Project> sortProject(@NotNull final List<Project> projectList,
                                               @Nullable final String sortMode) {
        return projectService.sortProject(projectList, sortMode);
    }
}