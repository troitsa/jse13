package ru.vlasova.iteco.taskmanager.service;

import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.comparator.*;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Setter
public final class ProjectService implements IProjectService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private ITaskService taskService;

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.taskService = serviceLocator.getTaskService();
    }

    @Override
    @Nullable
    public Project insert(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description, @Nullable final String dateStart,
                          @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || userId == null) return null;
        @NotNull final Project project = new Project(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUtil.parseDateFromString(dateStart));
        project.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) throws Exception {
        if (userId == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = getProjectByIndex(userId, index);
        if (project == null) return;
        repository.removeByUserId(userId, project.getId());
        session.commit();
        session.close();
        taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable Project project;
        try {
            project = repository.findOne(id);
            repository.removeByUserId(userId,id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Project was not removed");
        } finally {
            session.close();
        }
        if (project != null) taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    @Nullable
    public Project getProjectByIndex(@Nullable final String userId, final int index) throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        if (userId == null || index < 0 ) return null;
        @Nullable final List<Project> projectList = repository.findAllByUserId(userId);
        session.commit();
        @Nullable final Project project = projectList.get(index);
        session.close();
        return project;
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectIndex(@Nullable final String userId, final int projectIndex) throws Exception {
        if (userId == null || projectIndex < 0) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final List<Project> projectList = repository.findAllByUserId(userId);
        session.commit();
        @Nullable final String projectId = projectList.get(projectIndex).getId();
        session.close();
        return taskService.getTasksByProjectId(userId, projectId);
    }

    @Override
    @Nullable
    public List<Project> search(@Nullable final String userId, @Nullable final String searchString) throws Exception {
        if (userId == null || searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final List<Project> projectList = repository.search(userId, searchString);
        session.commit();
        session.close();
        return projectList;
    }

    @Override
    @Nullable
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final List<Project> projectList = repository.findAllByUserId(userId);
        session.commit();
        session.close();
        return projectList;
    }

    @Override
    public void merge(@Nullable final Project entity) throws Exception {
        if (entity == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.merge(entity);
            session.commit();
            return;
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(e);
        } finally {
            session.close();
        }
    }

    @Override
    @Nullable
    public Project persist(@Nullable final Project project) throws Exception {
        if (project == null) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.persist(project);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Project was not created");
        } finally {
            session.close();
        }
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final List<Project> projectList = repository.findAll();
        session.commit();
        session.close();
        return projectList;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = repository.findOne(id);
        session.commit();
        session.close();
        return project;
    }

    @Override
    @Nullable
    public Project findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = repository.findOneByUserId(userId,id);
        session.commit();
        session.close();
        if (project == null) return null;
        return project;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Project was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Projects was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAllByUserId(userId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Projects was not removed");
        } finally {
            session.close();
        }
    }

    @Override
    public @Nullable List<Project> sortProject(@NotNull final List<Project> projectList,
                                               @Nullable final String sortMode) {
        @Nullable Comparator<Project> comparator = new ByNameComparator<>();
        if(sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    comparator = new DateCreateComparator<>();
                    break;
                case ("2"):
                    comparator = new DateStartComparator<>();
                    break;
                case ("3"):
                    comparator = new DateFinishComparator<>();
                    break;
                case ("4"):
                    comparator = new ByStatusComparator<>();
                    break;
            }
        }
        Stream stream = projectList.stream();
        @Nullable final ArrayList<Project> list = (ArrayList<Project>) stream.sorted(comparator).collect(Collectors.toCollection(ArrayList::new));
        return list;
    }

}
