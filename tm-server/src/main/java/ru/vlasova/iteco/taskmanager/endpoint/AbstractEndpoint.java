package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;

@NoArgsConstructor
public class AbstractEndpoint {

    @Nullable
    @Getter
    protected ISessionService sessionService;

    public AbstractEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    protected void validateSession(@Nullable final String token) throws Exception {
        if (token == null) throw new Exception("You are not authorized");
        sessionService.validate(token);
    }

}
