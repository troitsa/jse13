package ru.vlasova.iteco.taskmanager.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.util.AESUtil;

import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull private ISessionService sessionService;

    public SessionEndpoint(@NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    public void removeSession(@Nullable final String token) throws Exception {
        sessionService.remove(token);
    }

    @Override
    public @Nullable Session findOneSession(@Nullable final String id) throws Exception {
        return sessionService.findOne(id);
    }

    @Nullable
    @Override
    public String getToken (@Nullable final String login, @Nullable String password) throws Exception {
        return sessionService.getToken(login, password);
    }

    @Override
    @Nullable
    public String getCurrentUserId(@Nullable final String token) throws Exception {
        return sessionService.getCurrentUserId(token);
    }
}
