package ru.vlasova.iteco.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("select * from taskmanager.app_project where user_id = #{userId} " +
            "and (name LIKE '%' #{searchString} '%' or description LIKE '%' #{searchString} '%')")
    List<Project> search(@NotNull @Param("userId") final String userId,
                         @NotNull @Param("searchString") final String searchString) throws Exception;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("select * from taskmanager.app_project where user_id = #{userId}")
    List<Project> findAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Delete("delete from taskmanager.app_project where user_id = #{userId}")
    void removeAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Select("select * from taskmanager.app_project where user_id = #{userId} and id = #{id}")
    Project findOneByUserId(@NotNull @Param("userId") final String userId,
                            @NotNull @Param("id") final String id) throws Exception;

    @Delete("delete from taskmanager.app_project where user_id = #{userId} and id = #{id}" )
    void removeByUserId(@NotNull @Param("userId") final String userId,
                @NotNull @Param("id") final String id) throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("select * from taskmanager.app_project")
    List<Project> findAll() throws Exception;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Select("select * from taskmanager.app_project where id = #{id}")
    Project findOne(@NotNull @Param("id") final String id) throws Exception;

    @Insert("insert into taskmanager.app_project " +
            "values (#{id},#{userId},#{name},#{description}, " +
            "#{dateCreate},#{dateStart},#{dateFinish},#{status})")
    void persist(@NotNull final Project obj) throws DuplicateException, SQLException;

    @Update("update taskmanager.app_project set user_id=#{userId}," +
            "name = #{name}, description = #{description}, " +
            "dateCreate = #{dateCreate}, dateStart= #{dateStart}, " +
            "dateFinish =#{dateFinish}, status = #{status} where id = #{id}")
    void merge(@NotNull final Project obj) throws SQLException;

    @Delete("delete from taskmanager.app_project where id = #{id}")
    void remove(@NotNull @Param("id") final String id) throws SQLException;

    @Delete("delete from taskmanager.app_project")
    void removeAll() throws SQLException;

}
